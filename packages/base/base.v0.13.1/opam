opam-version: "2.0"
maintainer: "opensource@janestreet.com"
authors: ["Jane Street Group, LLC <opensource@janestreet.com>"]
homepage: "https://github.com/janestreet/base"
bug-reports: "https://github.com/janestreet/base/issues"
dev-repo: "git+https://github.com/janestreet/base.git"
doc: "https://ocaml.janestreet.com/ocaml-core/latest/doc/base/index.html"
license: "MIT"
build: [
  ["dune" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml"    {>= "4.04.2"}
  "sexplib0" {>= "v0.13" & < "v0.14"}
  "dune"     {>= "1.5.1"}
  "dune-configurator"
]
synopsis: "Full standard library replacement for OCaml"
description: "
Full standard library replacement for OCaml

Base is a complete and portable alternative to the OCaml standard
library. It provides all standard functionalities one would expect
from a language standard library. It uses consistent conventions
across all of its module.

Base aims to be usable in any context. As a result system dependent
features such as I/O are not offered by Base. They are instead
provided by companion libraries such as stdio:

  https://github.com/janestreet/stdio
"
url {
  src: "https://github.com/janestreet/base/archive/v0.13.1.tar.gz"
  checksum: [
    "md5=296457416f9a8b75e6edfc3b1140e384"
    "sha256=f2dc50c724669b278428cc4c9c742f754049ebe59537b984377b0a777e677b50"
    "sha512=47a8e5c4159f0b2bc31e443749aeb1782a1a56c931b951e96cea764237ed6d450b991eeb2994cfb30f6e55fb571e2a20549a21a2b4e66a76ba745af286214c12"
  ]
}
